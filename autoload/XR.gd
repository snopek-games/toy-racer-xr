extends Node

enum XRMode {
	VR,
	AR,
}

var perform_runtime_config = false

var arvr_interface
var arvr_interface_name := "none"

var ovr_init_config
var ovr_performance

var button_mapping := {
	trigger = -1,
	grip = -1,
	button_1 = -1,
	button_2 = -1,
}

var device_mapping := {
	left = -1,
	right = -1,
}

var axis_mapping := {
	thumbstick_x = -1,
}

signal webxr_supported_sessions (vr_supported, ar_supported)
signal webxr_session_started ()
signal webxr_session_ended ()

func setup() -> bool:
	if OS.get_name() == 'Android':
		arvr_interface = ARVRServer.find_interface("OVRMobile")
		if arvr_interface:
			arvr_interface_name = "OVRMobile"
			ovr_init_config = preload("res://addons/godot_ovrmobile/OvrInitConfig.gdns").new()
			ovr_init_config.set_render_target_size_multiplier(1)
			
			ovr_performance = preload("res://addons/godot_ovrmobile/OvrPerformance.gdns").new()
			
			button_mapping = {
				trigger = 15,
				grip = 2,
				button_1 = 7,
				button_2 = 1,
			}
			
			axis_mapping = {
				thumbstick_x = 0,
			}
			
			device_mapping = {
				left = 0,
				right = 1,
			}
			
			if arvr_interface.initialize():
				get_viewport().arvr = true
				return true
	elif OS.get_name() == 'HTML5':
		arvr_interface = ARVRServer.find_interface("WebXR")
		if arvr_interface:
			arvr_interface_name = "WebXR"
			arvr_interface.connect("session_started", self, "_webxr_session_started")
			arvr_interface.connect("session_ended", self, "_webxr_session_ended")
			arvr_interface.connect("session_failed", self, "_webxr_session_failed")
			
			button_mapping = {
				trigger = 0,
				grip = 1,
				button_1 = 4,
				button_2 = 5,
			}
			
			axis_mapping = {
				thumbstick_x = 2
			}
			
			device_mapping = {
				left = 100,
				right = 101,
			}
			
			_webxr_check_supported_sessions()
			
			return true
	
	return false

func _webxr_check_supported_sessions():
	var result: Array
	var vr_supported: bool
	var ar_supported: bool
	
	arvr_interface.is_session_supported("immersive-vr")
	result = yield(arvr_interface, "session_supported")
	vr_supported = result[1]
	
	arvr_interface.is_session_supported("immersive-ar")
	result = yield(arvr_interface, "session_supported")
	ar_supported = result[1]
	
	emit_signal("webxr_supported_sessions", vr_supported, ar_supported)

func initialize_webxr(xr_mode = XRMode.VR) -> bool:
	if arvr_interface_name == "WebXR":
		arvr_interface.session_mode = 'immersive-vr' if xr_mode == XRMode.VR else 'immersive-ar'
		arvr_interface.required_features = 'local-floor'
		arvr_interface.optional_features = 'bounded-floor'
		arvr_interface.requested_reference_space_types = 'bounded-floor, local-floor, local'	
		return arvr_interface.initialize()
	
	return false

func _process(_delta):
	if not perform_runtime_config and ovr_performance:
		ovr_performance.set_clock_levels(1, 1)
		ovr_performance.set_extra_latency_mode(1)
		perform_runtime_config = true

func _webxr_session_started():
	get_viewport().arvr = true
	emit_signal("webxr_session_started")

func _webxr_session_ended():
	get_viewport().arvr = false
	emit_signal("webxr_session_ended")

func _webxr_session_failed(message):
	OS.alert("Failed to initialize VR: " + str(message))
	emit_signal("webxr_session_ended")
