Toy Racer VR
============

This is a little VR demo, created to test out WebXR with Godot.

Godot's WebXR support is a work-in-progress - see the current PR:

https://github.com/godotengine/godot/pull/42397

This also works using Godot's Oculus Mobile plugin on the Oculus Quest.

