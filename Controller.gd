extends ARVRController

func _ready() -> void:
	self.connect("button_pressed", self, "_on_button_pressed")
	self.connect("button_release", self, "_on_button_release")

func _on_button_pressed(button: int) -> void:
	if button == XR.button_mapping['trigger']:
		$ControllerModel.trigger_pulled = true

func _on_button_release(button: int) -> void:
	if button == XR.button_mapping['trigger']:
		$ControllerModel.trigger_pulled = false
