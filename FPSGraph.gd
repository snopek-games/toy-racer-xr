extends Control

export (int) var total_samples = 30

var max_fps := 0
var samples := []

func add_sample(fps, min_fps) -> void:
	max_fps = max(max_fps, fps)
	samples.append([ fps, min_fps ])
	if samples.size() > total_samples:
		samples.pop_front()
	update()

func clear() -> void:
	max_fps = 0
	samples.clear()
	update()

func _draw() -> void:
	var full_interval: float = rect_size.x / total_samples
	var half_interval: float = full_interval / 2.0
	
	# Give some space at the top.
	var max_y = max_fps + 10
	
	var start_x = rect_size.x - (samples.size() * full_interval)
	var previous_sample = null
	for sample in samples:
		var min_fps_height = rect_size.y * (float(sample[1]) / max_y)
		draw_rect(
			Rect2(start_x, rect_size.y - min_fps_height, full_interval, min_fps_height),
			Color(0.0, 1.0, 0.0, 1.0),
			true)
		
		var fps_y = rect_size.y - (rect_size.y * (float(sample[0]) / max_y))
		draw_circle(Vector2(start_x + half_interval, fps_y), 6.0, Color(1.0, 0.0, 0.0, 1.0))
		
		if previous_sample:
			draw_line(
				Vector2(start_x - half_interval, rect_size.y - (rect_size.y * (float(previous_sample[0]) / max_y))),
				Vector2(start_x + half_interval, fps_y),
				Color(1.0, 0.0, 0.0, 1.0),
				true,
				3.0)
		
		previous_sample = sample
		start_x += full_interval
