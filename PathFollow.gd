extends PathFollow

var car_going := false
var car_speed := 0.0
var car_max_speed := 5.0
var car_acceleration := 10.0
var car_friction := 10.0

var car: VehicleBody

func _ready() -> void:
	for child in get_children():
		if child is VehicleBody:
			car = child
			break

func _process(_delta):
	if car_going:
		car_speed += car_acceleration * _delta
	else:
		car_speed -= car_friction * _delta
	car_speed = clamp(car_speed, 0.0, car_max_speed)
	
	var previous_offset = offset
	offset += (car_speed * _delta)
	
	# Hack to reset rotation on every loop, to prevent the car slowly
	# rotating a little bit over time.
	if offset < previous_offset:
		_reset_rotation()

func reset() -> void:
	offset = 0.0
	_reset_rotation()

func _reset_rotation() -> void:
	rotation_degrees = Vector3(0, 0, 0)
