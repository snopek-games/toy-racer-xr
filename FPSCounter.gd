extends Spatial

onready var fps_counter_display := $Viewport/FPSCounterDisplay

var enabled := true setget set_enabled

func set_enabled(_enabled: bool) -> void:
	if enabled != _enabled:
		enabled = _enabled
		
		visible = enabled
		fps_counter_display.set_process(enabled)
		fps_counter_display.clear()
