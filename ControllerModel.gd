extends Spatial

export (bool) var trigger_pulled := false setget set_trigger_pulled

func set_trigger_pulled(_trigger_pulled) -> void:
	if trigger_pulled != _trigger_pulled:
		trigger_pulled = _trigger_pulled
		
		if trigger_pulled:
			$AnimationPlayer.play("PullTrigger")
		else:
			$AnimationPlayer.play_backwards("PullTrigger")

