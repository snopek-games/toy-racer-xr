#!/bin/bash

ANDROID_RELEASE_KEYSTORE_PATH="$(pwd)/$EXPORT_NAME.keystore"

if [ ! -e "$ANDROID_RELEASE_KEYSTORE_PATH" ]; then
	echo "$ANDROID_RELEASE_KEYSTORE" | base64 --decode > $ANDROID_RELEASE_KEYSTORE_PATH
else
	echo "Warning: the keystore file already exists"
fi

set_export_value() {
	sed -i -e "s,^$1=.*$,$1=\"$2\"," export_presets.cfg
}

set_export_value keystore/release "$ANDROID_RELEASE_KEYSTORE_PATH"
set_export_value keystore/release_user "$ANDROID_RELEASE_USERNAME"
set_export_value keystore/release_password "$ANDROID_RELEASE_PASSWORD"

