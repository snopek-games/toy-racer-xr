extends Spatial

onready var path_follow := $Path/PathFollow
onready var car = path_follow.car
onready var car_start_transform = path_follow.global_transform

export var joy_device := 0 setget set_joy_device
export var steering_axis := 0 setget set_steering_axis

var using_path := true
var trigger_pressed := false
var grip_pressed := false

func _ready() -> void:
	car.joy_device = joy_device
	car.joy_steering = steering_axis
	
	XR.connect("webxr_session_started", self, "_setup_webxr")

func _setup_webxr() -> void:
	XR.arvr_interface.connect("selectstart", self, "_on_webxr_select", [true])
	XR.arvr_interface.connect("selectend", self, "_on_webxr_select", [false])
	XR.arvr_interface.connect("squeezestart", self, "_on_webxr_squeeze", [true])
	XR.arvr_interface.connect("squeezeend", self, "_on_webxr_squeeze", [false])
	
	# We only need to do this once.
	XR.disconnect("webxr_session_started", self, "_setup_webxr")

func set_joy_device(_joy_device):
	if joy_device != _joy_device:
		joy_device = _joy_device
		if car:
			car.joy_device = joy_device

func set_steering_axis(_steering_axis):
	if steering_axis != _steering_axis:
		steering_axis = _steering_axis
		if car:
			car.joy_steering = steering_axis

func _reset_car() -> void:
	car.global_transform = car_start_transform
	car.angular_velocity = Vector3.ZERO
	car.linear_velocity = Vector3.ZERO

func _on_controller_button_pressed(button: int) -> void:
	#print("pressed: " + str(button))
	if button == XR.button_mapping['trigger']:
		trigger_pressed = true
	elif button == XR.button_mapping['grip']:
		grip_pressed = true
	elif button == XR.button_mapping['button_1']:
		if using_path:
			path_follow.reset()
		else:
			call_deferred('_reset_car')

func _on_controller_button_release(button: int) -> void:
	#print("released: " + str(button))
	if button == XR.button_mapping['trigger']:
		trigger_pressed = false
	elif button == XR.button_mapping['grip']:
		grip_pressed = false

func _on_webxr_select(controller_id: int, pressed: bool) -> void:
	if controller_id == joy_device - 99:
		print ("select (%s): %s" % [controller_id, pressed])
		trigger_pressed = pressed

func _on_webxr_squeeze(controller_id: int, pressed: bool) -> void:
	if controller_id == joy_device - 99:
		print ("squeeze (%s): %s" % [controller_id, pressed])
		grip_pressed = pressed

func _physics_process(delta: float) -> void:
	if using_path:
		path_follow.car_going = trigger_pressed
	else:
		car.joy_driving = true
		if trigger_pressed:
			car.throttle_val = 1.0
		elif grip_pressed:
			car.throttle_val = -1.0
		else:
			car.throttle_val = 0.0

func _unhandled_input(event: InputEvent) -> void:
#	if event is InputEventJoypadMotion:
#		print ("device: " + str(event.device))
#		print ("axis: " + str(event.axis))
#		print ("axis value: " + str(event.axis_value))
	
	if event is InputEventJoypadMotion and event.device == joy_device and event.axis == steering_axis and using_path:
		# Switch to using direct input.
		using_path = false
		var car_transform = path_follow.global_transform
		path_follow.remove_child(car)
		add_child(car)
		path_follow.queue_free()
		car.global_transform = car_transform

