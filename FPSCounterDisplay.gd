extends Control

onready var label := $VBoxContainer/Label
onready var graph := $VBoxContainer/Graph

var _prev_frame_time: int = 0
var _min_fps: int = 1000
var _frames: int = 0
var _total_ticks: int = 0

func _process(_delta: float) -> void:
	var time = OS.get_ticks_usec()
	var frame_ticks: int = time - _prev_frame_time
	var frame_fps: int = round(1000000.0 / frame_ticks)
	_prev_frame_time = time
	
	_total_ticks += frame_ticks
	_min_fps = min(_min_fps, frame_fps)
	_frames += 1
	
	if _total_ticks >= 1000000:
		update_display(_frames, _min_fps)
		
		_total_ticks = _total_ticks % 1000000
		_frames = 0
		_min_fps = 1000

func update_display(fps, min_fps) -> void:
	label.text = "%s fps (min: %s fps)" % [fps, min_fps]
	graph.add_sample(fps, min_fps)

func clear() -> void:
	_frames = 0
	_total_ticks = 0
	_min_fps = 1000
	
	graph.clear()
