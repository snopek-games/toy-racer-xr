extends VehicleBody

############################################################
# behaviour values

export var MAX_ENGINE_FORCE = 20.0
export var MAX_BRAKE_FORCE = 0.5
export var MAX_STEER_ANGLE = 0.5

export var steer_speed = 5.0

var steer_target = 0.0
var steer_angle = 0.0

############################################################
# Input

export var joy_driving := true setget set_joy_driving
export var joy_device = 0
export var joy_steering = JOY_ANALOG_LX
export var steering_mult = -1.0

var throttle_val = 0.0
var brake_val = 0.0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func set_joy_driving(_joy_driving):
	if joy_driving != _joy_driving:
		joy_driving = _joy_driving
		if joy_driving:
			mode = RigidBody.MODE_RIGID
		else:
			mode = RigidBody.MODE_KINEMATIC

func _physics_process(delta):
	if not joy_driving:
		return
	
	var steer_val = steering_mult * Input.get_joy_axis(joy_device, joy_steering)
	
	# overrules for keyboard debugging
#	if Input.is_action_pressed("debug_up"):
#		throttle_val = 1.0
#	if Input.is_action_pressed("debug_down"):
#		brake_val = 1.0
#	if Input.is_action_pressed("debug_left"):
#		steer_val = 1.0
#	elif Input.is_action_pressed("debug_right"):
#		steer_val = -1.0

	engine_force = throttle_val * MAX_ENGINE_FORCE
	brake = brake_val * MAX_BRAKE_FORCE
	
	steer_target = steer_val * MAX_STEER_ANGLE
	if (steer_target < steer_angle):
		steer_angle -= steer_speed * delta
		if (steer_target > steer_angle):
			steer_angle = steer_target
	elif (steer_target > steer_angle):
		steer_angle += steer_speed * delta
		if (steer_target < steer_angle):
			steer_angle = steer_target
	
	steering = steer_angle
