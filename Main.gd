extends Spatial

onready var world_environment = $WorldEnvironment
onready var ui = $UI
onready var enter_vr_button = $UI/VBoxContainer/EnterVRButton
onready var enter_ar_button = $UI/VBoxContainer/EnterARButton
onready var fps_counter = $FPSCounter

func _ready():
	XR.connect("webxr_supported_sessions", self, "_on_webxr_supported_sessions")
	XR.connect("webxr_session_started", self, "_on_webxr_session_started")
	XR.connect("webxr_session_ended", self, "_on_webxr_session_ended")
	
	if XR.setup():
		$Track/CarController1.joy_device = XR.device_mapping['left']
		$Track/CarController1.steering_axis = XR.axis_mapping['thumbstick_x']
		$Track/CarController2.joy_device = XR.device_mapping['right']
		$Track/CarController2.steering_axis = XR.axis_mapping['thumbstick_x']
	else:
		$OverheadCamera.current = true
	
	fps_counter.enabled = false

func _on_webxr_supported_sessions(vr_supported, ar_supported) -> void:
	if not vr_supported and not ar_supported:
		OS.alert("Your browser (or connected hardware) doesn't support either AR or VR")
		return
	
	ui.visible = true
	
	if vr_supported:
		enter_vr_button.visible = true
	if ar_supported:
		enter_ar_button.visible = true

func _on_webxr_session_started() -> void:
	ui.visible = false

func _on_webxr_session_ended() -> void:
	var viewport = get_viewport()
	viewport.arvr = false
	viewport.transparent_bg = false
	viewport.fxaa = true
	
	world_environment.environment.background_mode = Environment.BG_SKY
	
	ui.visible = true

func _on_EnterVRButton_pressed() -> void:
	if not XR.initialize_webxr(XR.XRMode.VR):
		OS.alert("Failed to initialize VR")

func _on_EnterARButton_pressed() -> void:
	if not XR.initialize_webxr(XR.XRMode.AR):
		OS.alert("Failed to initialize AR")
	
	var viewport = get_viewport()
	
	world_environment.environment.background_mode = Environment.BG_COLOR
	world_environment.environment.background_color = Color(0.0, 0.0, 0.0, 0.0)
	viewport.transparent_bg = true
	
	# FXAA will cause the background to be black, rather than show what the
	# camera is seeing.
	viewport.fxaa = false

func _on_Controller_button_pressed(button: int) -> void:
	if button == XR.button_mapping['button_2']:
		fps_counter.enabled = !fps_counter.enabled
